package ru.hayova.cache.api;

//**********************************
//* (c) Oleg 'hayova' Nadein, 2016 *
//**********************************

public class CacheDefs {
	public static final String CACHE_DIRECTORY = "localcache/";
	
	public static final Integer CACHE_DEFAULT_MAX_SIZE = 10;
	
	public static final Integer CACHE_STRATEGY_ONLY_MEMORY = 1;
	public static final Integer CACHE_STRATEGY_ONLY_FILES = 2;
	public static final Integer CACHE_STRATEGY_MEMORY_AND_FILES = 3;
	
	
}
