package ru.hayova.cache.api;

//**********************************
//* (c) Oleg 'hayova' Nadein, 2016 *
//**********************************

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FileCache<Key, Value> implements CacheInterface<Key, Value> {

    private Integer itemsCount = 0;

    public FileCache() {
    	// only for test
    	clear();
    } 
    
    private File getFileByKey(Key key) {
    	File directory = new File(CacheDefs.CACHE_DIRECTORY);
    	if (directory.exists()) {
	        File[] files = directory.listFiles();
	        for (File file : files) {
	            Entry<Key, Value> entry = loadFromFile(file);
	
	            if (entry.getKey().equals(key)) {
	                return file;
	            }
	        }
    	}
        return null;
    }
    
    @Override
    public void add(Key key, Value value) {
        File dir = new File(CacheDefs.CACHE_DIRECTORY);

        File fileToSave = null;

        if (!dir.exists()) {
            dir.mkdirs();
            fileToSave = new File(CacheDefs.CACHE_DIRECTORY + UUID.randomUUID().toString());
            itemsCount++;
        } else {
        	fileToSave = getFileByKey(key);
            if (fileToSave == null) {
            	fileToSave = new File(CacheDefs.CACHE_DIRECTORY + UUID.randomUUID().toString());
            	itemsCount++;
            }

        }

        saveToFile(key, value, fileToSave);
    } 
    
    @Override
    public Value get(Key key) {
        File fileToGet = getFileByKey(key);

        if (fileToGet == null)
            return null;

        Value result = loadFromFile(fileToGet).getValue();

        return result;
    } 
    
    @Override
    public void remove(Key key) {
        File directory = new File(CacheDefs.CACHE_DIRECTORY);

        if (directory.exists()) {
            File fileToRemove = getFileByKey(key);

            if (fileToRemove != null) {
            	fileToRemove.delete();
                itemsCount--;
            }
        }
    } 
    
    @Override
    public void clear() {
    	File directory = new File(CacheDefs.CACHE_DIRECTORY);
    	if (directory.exists()) {
	        for (File file : directory.listFiles()) {
	            if (!file.isDirectory())
	                file.delete();
	        }
    	}
    }

    @Override
    public int size() {
        return this.itemsCount;
    } 
    
    // only for test
    @Override
	public List<Key> keyList() {
    	List<Key> list = new ArrayList<Key>();
      	File directory = new File(CacheDefs.CACHE_DIRECTORY);
        for (File file : directory.listFiles()) {
            if (!file.isDirectory()) {
            	Entry<Key, Value> entry = loadFromFile(file);
            	list.add(entry.getKey());
            }
        }
        return list;
	}
    
    public void saveToFile(Key key, Value value, File file) {
        OutputStream outputStream = null;
        ObjectOutputStream objectOutputStream = null;
        try {
        	outputStream = new BufferedOutputStream(new FileOutputStream(file));
        	objectOutputStream = new ObjectOutputStream(outputStream);

        	objectOutputStream.writeObject(key);
        	objectOutputStream.writeObject(value);
        	objectOutputStream.flush();
        } catch (IOException e) {
            throw new RuntimeException("IO exception while file saving", e);
        } finally {
        	try {
	        	if (outputStream != null) {
	        		outputStream.close();
	            }
	        	if (objectOutputStream != null) {
	        		objectOutputStream.close();
	            }
	        } catch (IOException e) {}
        }
    }
    
    @SuppressWarnings("unchecked")
    public Entry<Key, Value> loadFromFile(File file) {
        Entry<Key, Value> result = null;
        InputStream inputStream = null;
        ObjectInputStream objectInputStream = null;

        try {
        	inputStream = new BufferedInputStream(new FileInputStream(file));
        	objectInputStream = new ObjectInputStream(inputStream);
            Key key = (Key) objectInputStream.readObject();
            Value value = (Value) objectInputStream.readObject();
            result = new Entry<Key, Value>(key, value);
        } catch (IOException e) {
            throw new RuntimeException("IO exception while file load", e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Class not found while file load", e);
        } finally {
        	try {
	        	if (inputStream != null) {
	        		inputStream.close();
	            }
	        	if (objectInputStream != null) {
	        		objectInputStream.close();
	            }
	        } catch (IOException e) {}
        }

        return result;
    }

	private static class Entry<Key, Value> {
        private Key key;
        private Value value;

        private Entry(Key key, Value value) {
            this.setKey(key);
            this.setValue(value);
        }

		public Key getKey() {
			return key;
		}

		public void setKey(Key key) {
			this.key = key;
		}

		public Value getValue() {
			return value;
		}

		public void setValue(Value value) {
			this.value = value;
		}
    }
	
    public int getItemsCount() {
		return itemsCount;
	}

	public void setItemsCount(int itemsCount) {
		this.itemsCount = itemsCount;
	}
	
 
}
