package ru.hayova.cache.api;

//**********************************
//* (c) Oleg 'hayova' Nadein, 2016 *
//**********************************

import java.util.List;

public interface CacheInterface<Key, Value> {
    void add(Key key, Value value);
    Value get(Key key);
    void remove(Key key);
    void clear();
    int size();
    // only for test
    List<Key> keyList();
}