package ru.hayova.cache.api;

//**********************************
//* (c) Oleg 'hayova' Nadein, 2016 *
//**********************************

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MemoryCache<Key, Value> implements CacheInterface<Key, Value> {
	   
	private final Map<Key, Value> map = new HashMap<Key, Value>();

	public MemoryCache() {
		
	}
	
	@Override
    public void add(Key key, Value value) {
        map.put(key, value);
    }

	@Override
    public Value get(Key key) {
        if (map.containsKey(key))          
        	return map.get(key);
        else
        	return null;
    }

	@Override
    public void clear() {
        map.clear();
    }

	@Override
    public void remove(Key key) {
        map.remove(key);
    }

	@Override
    public int size() {
        return map.size();
    }
    
	// only for test
    @Override
	public List<Key> keyList() {
    	List<Key> list = new ArrayList<Key>();
    	for (Key key : map.keySet()) {
    		list.add(key);
    	}
    	return list;
    }
    
}
