package ru.hayova.cache.api;

// **********************************
// * (c) Oleg 'hayova' Nadein, 2016 *
// **********************************

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@SuppressWarnings("rawtypes")
public class Cache<Key, Value> implements CacheInterface<Key, Value> {

	private Integer strategy;
	private Integer maxCacheSize;
	private CacheInterface cacheInterfaceMemory;
	private CacheInterface cacheInterfaceFiles;
	
	private Map<Key, Integer> frequencies = new HashMap<Key, Integer>();
	
	public Cache() {
		this.setStrategy(CacheDefs.CACHE_STRATEGY_MEMORY_AND_FILES);
		this.setMaxCacheSize(CacheDefs.CACHE_DEFAULT_MAX_SIZE);
		initCaches();
	}
	
	public Cache(Integer strategy, Integer maxCacheSize) {
		this.setStrategy(strategy);
		this.setMaxCacheSize(maxCacheSize);
		initCaches();
	}

	private void initCaches() {
		cacheInterfaceMemory = new MemoryCache<Key, Value>();
		cacheInterfaceFiles = new FileCache<Key, Value>();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void add(Key key, Value value) {	
		if(this.strategy == CacheDefs.CACHE_STRATEGY_ONLY_MEMORY) {
			if(cacheInterfaceMemory.size() < this.maxCacheSize)
				cacheInterfaceMemory.add(key, value);
		} else if(this.strategy == CacheDefs.CACHE_STRATEGY_ONLY_FILES) {
			if(cacheInterfaceFiles.size() < this.maxCacheSize)
				cacheInterfaceFiles.add(key, value);
		} else if(this.strategy == CacheDefs.CACHE_STRATEGY_MEMORY_AND_FILES) {
			if(cacheInterfaceMemory.size() < this.maxCacheSize)
				cacheInterfaceMemory.add(key, value);
			else {
				if(cacheInterfaceFiles.size() < this.maxCacheSize)
					cacheInterfaceFiles.add(key, value);
			}
		}
	}

	@Override
	public Value get(Key key) {
		Value value = getValueData(key);	
		if(value != null)
			addFrequency(key);
		
		return value;
	}
	
	@SuppressWarnings("unchecked")
	private Value getValueData(Key key) {
		Value value = null;
		if(this.strategy == CacheDefs.CACHE_STRATEGY_ONLY_MEMORY) {
			value = (Value) cacheInterfaceMemory.get(key);
		} else if(this.strategy == CacheDefs.CACHE_STRATEGY_ONLY_FILES) {
			value = (Value) cacheInterfaceFiles.get(key);
		} else if(this.strategy == CacheDefs.CACHE_STRATEGY_MEMORY_AND_FILES) {
			value = (Value) cacheInterfaceMemory.get(key);
			if(value == null)
				value = (Value) cacheInterfaceFiles.get(key);
		}
		
		return value;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void remove(Key key) {
		if(this.strategy == CacheDefs.CACHE_STRATEGY_ONLY_MEMORY || this.strategy == CacheDefs.CACHE_STRATEGY_MEMORY_AND_FILES)
			cacheInterfaceMemory.remove(key);
		if(this.strategy == CacheDefs.CACHE_STRATEGY_ONLY_FILES || this.strategy == CacheDefs.CACHE_STRATEGY_MEMORY_AND_FILES)
			cacheInterfaceFiles.remove(key);
	}

	@Override
	public void clear() {
		if(this.strategy == CacheDefs.CACHE_STRATEGY_ONLY_MEMORY || this.strategy == CacheDefs.CACHE_STRATEGY_MEMORY_AND_FILES)
			cacheInterfaceMemory.clear();
		if(this.strategy == CacheDefs.CACHE_STRATEGY_ONLY_FILES || this.strategy == CacheDefs.CACHE_STRATEGY_MEMORY_AND_FILES)
			cacheInterfaceFiles.clear();
	}

	@Override
	public int size() {
		if(this.strategy == CacheDefs.CACHE_STRATEGY_ONLY_MEMORY) {
			return cacheInterfaceMemory.size();
		} else if(this.strategy == CacheDefs.CACHE_STRATEGY_ONLY_FILES) {
			return cacheInterfaceFiles.size();
		} else if(this.strategy == CacheDefs.CACHE_STRATEGY_MEMORY_AND_FILES) {
			return (cacheInterfaceMemory.size() + cacheInterfaceFiles.size());
		} else {
			return -1;
		}
	}
	
	// only for test
    @SuppressWarnings("unchecked")
	@Override
	public List<Key> keyList() {
    	if(this.strategy == CacheDefs.CACHE_STRATEGY_ONLY_MEMORY || this.strategy == CacheDefs.CACHE_STRATEGY_MEMORY_AND_FILES) {
	    	System.out.println("=== START MEMORY CACHE LISTING ===");
	    	List<Key> list1 = cacheInterfaceMemory.keyList();
	    	for(Key key : list1) {
	    		System.out.println("key " + key.toString() + ", freq " + frequencies.get(key));
	    	}
	    	System.out.println("==== END MEMORY CACHE LISTING ====");
    	}
    	
    	if(this.strategy == CacheDefs.CACHE_STRATEGY_ONLY_FILES || this.strategy == CacheDefs.CACHE_STRATEGY_MEMORY_AND_FILES) {
	    	System.out.println("=== START FILE CACHE LISTING ===");
	    	List<Key> list2 = cacheInterfaceFiles.keyList();
	    	for(Key key : list2) {
	    		System.out.println("key " + key.toString() + ", freq " + frequencies.get(key));
	    	}
	    	System.out.println("==== END FILE CACHE LISTING ====");
    	}
    	
    	return null;
    }
	
	@SuppressWarnings("unchecked")
	public void rebuildCaches() {
		if(this.strategy == CacheDefs.CACHE_STRATEGY_MEMORY_AND_FILES) {
			List<Key> sortedKeys = sortFrequencies();
			if(sortedKeys.size() > 0) {
				Integer endMemoryList = (sortedKeys.size() > this.maxCacheSize ? this.maxCacheSize-1 : sortedKeys.size()-1);
				
				for(Key key : sortedKeys.subList(0, endMemoryList)) {
			        Value value = getValueData(key);
			        if(value != null) {
			        	remove(key);
			            cacheInterfaceMemory.add(key, value);
			        }
				}
				
				if(sortedKeys.size() > this.maxCacheSize) {
					Integer endFileList = ((sortedKeys.size() > (this.maxCacheSize*2)) ? (this.maxCacheSize*2)-1 : sortedKeys.size() - 1);
					for(Key key : sortedKeys.subList(endMemoryList + 1, endFileList)) {
			            Value value = getValueData(key);
			            if(value != null) {
			            	remove(key);
			            	cacheInterfaceFiles.add(key, value);
			            }
					}		
				}
			}
		}
	}
	
	private void addFrequency(Key key) {
	    if (frequencies.containsKey(key)) {
	        Integer frequency = frequencies.get(key);
	        frequency++;
	        frequencies.put(key, frequency);
	    } else {
	    	frequencies.put(key, 1);
	    }
	}
	
	private List<Key> sortFrequencies() {
        List<java.util.Map.Entry<Key, Integer>> freqSet = new ArrayList<Map.Entry<Key, Integer>>(frequencies.entrySet());

        Collections.sort(freqSet, new Comparator<java.util.Map.Entry<Key, Integer>>() {
			@Override
			public int compare(Entry<Key, Integer> o1, Entry<Key, Integer> o2) {
				return -o1.getValue().compareTo(o2.getValue());
			}
        });

        List<Key> result = new ArrayList<Key>();

        for (int i = 0; i < frequencies.size(); ++i) {
            result.add(freqSet.get(i).getKey());      
        }
    
        return result;
    } 
	
	public Integer getStrategy() {
		return strategy;
	}

	public void setStrategy(Integer strategy) {
		this.strategy = strategy;
	}

	public Integer getMaxCacheSize() {
		return maxCacheSize;
	}

	public void setMaxCacheSize(Integer maxCacheSize) {
		this.maxCacheSize = maxCacheSize;
	}

	public CacheInterface getCacheInterfaceMemory() {
		return cacheInterfaceMemory;
	}

	public void setCacheInterfaceMemory(CacheInterface cacheInterfaceMemory) {
		this.cacheInterfaceMemory = cacheInterfaceMemory;
	}

	public CacheInterface getCacheInterfaceFiles() {
		return cacheInterfaceFiles;
	}

	public void setCacheInterfaceFiles(CacheInterface cacheInterfaceFiles) {
		this.cacheInterfaceFiles = cacheInterfaceFiles;
	}

	public Map<Key, Integer> getFrequencies() {
		return frequencies;
	}

	public void setFrequencies(Map<Key, Integer> frequencies) {
		this.frequencies = frequencies;
	}
	
}
