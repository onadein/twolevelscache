package ru.hayova.cache.app;

//**********************************
//* (c) Oleg 'hayova' Nadein, 2016 *
//**********************************

import java.util.Random;

import ru.hayova.cache.api.Cache;
import ru.hayova.cache.api.CacheDefs;

public class CacheTestApp {
	
	public static void main(String[] args) {
        Cache<String, String> cache = new Cache<String, String>(CacheDefs.CACHE_STRATEGY_MEMORY_AND_FILES, CacheDefs.CACHE_DEFAULT_MAX_SIZE);
        
        Integer addValuesCount = CacheDefs.CACHE_DEFAULT_MAX_SIZE * 3;
        Integer readValuesCount = CacheDefs.CACHE_DEFAULT_MAX_SIZE * 20;
        
        System.out.println("Adding " + addValuesCount + " values to cache...");
        
        for(int i = 0; i < addValuesCount; i++) {
        	cache.add("key" + i, "value" + i);
        }

        System.out.println("Read " + readValuesCount +  " values from cache...");
        
        for(int i = 0; i < readValuesCount; i++) {
        	Random generator = new Random(); 
        	Integer randomInt = generator.nextInt(CacheDefs.CACHE_DEFAULT_MAX_SIZE * 3);
        	cache.get("key" + randomInt);
        }

        cache.keyList();
     
        System.out.println("Rebuild caches...");
        
        cache.rebuildCaches();

        cache.keyList();
        
    } 
	
}
